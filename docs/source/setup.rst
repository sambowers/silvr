Setup instructions
==================

Requirements
------------

silvR is a set of R functions written for use in Linux or Windows. None of the functions of silvR should require a powerful PC to run, though some will require an internet connection to function properly.

Functions in silvR make use of a common R libraries for data manipulation (dplyr, tidyr), ecological analysis (vegan), geospatial data manipulation (raster, rgdal, sp), and a specialised library for forest plot data analysis (BIOMASS).

Installing silvR
----------------

To install silvR, you'll first have to install and load devtools as follows:

.. code-block:: R
    
    > install.packages('devtools')    
    
    > library('devtools')

Once you have devtools, you can install silvR as follows:

.. code-block:: R
    
    > install_bitbucket('sambowers/silvr')

To load the silvR functions, run the following:

.. code-block:: R
    
    > library('silvr')

Loading data
------------

For the purposes of this documentation, we'll be making use of a synthetic forest inventory dataset. The dataset contains stems from 25 forest inventory plots of 0.25 ha located in miombo woodlands.

The datasets are packaged with silvr, and are available as follows:

.. code-block:: R
    
    > data(mstems) # Stem data, one row per tree stem
        
    > data(mplots) # Plot data, one row per plot
    
Take a peek at these two datasets using the ``head()`` function before continuing:

.. code-block:: R
    
    > head(mstems)
    
    > head(mplots)
Working with real data
----------------------

Thus far, we've been using synthetic forest inventory data. This is great for learning, but doesn't simulate the difficulties that are associated with real-world, messy data.

Here we'll look at inventory data from a 50 year fire experiment in Marondera, Zimbabwe. The experiment was designed to look at the effect of frequent fires on tree populations. The photos below show what the plots looked like after being burned every year (top;  fire return interval = 1 year) and after complete protection from fire (below; fire return interval = 50 years). The experiment had a number of plots subjected to a range of different fire treatments.

For more on the experiment see:

*Furley, Rees, Ryan and Saiz (2009) Savanna burning and the assessment of long-term fire experiments with particular reference to Zimbabwe. Progress in Physical Geography 32(6): 611-634*

.. image:: images/marondera1.jpg
   :width: 400px
   :align: center

.. image:: images/marondera2.jpg
   :width: 400px
   :align: center

We'll use this dataset to answer the following questions:

#. What is the relationship between fire return interval and aboveground biomass?
#. What effect does fire have on species composition?

Dataset description
~~~~~~~~~~~~~~~~~~~

The data are provided in ``.csv`` format, containing five columns:


+--------------+--------------------------------------------------------------------------------------------------------------+
| Parameter    | Description                                                                                                  |
+--------------+--------------------------------------------------------------------------------------------------------------+
| plot_code    | A unique ID for each experimental plot.                                                                      |
+--------------+--------------------------------------------------------------------------------------------------------------+
| species_name | Species ID in the fomat ‘Genus species’.                                                                     |
+--------------+--------------------------------------------------------------------------------------------------------------+
| DBH          | Diameter at breast height, in units of centimetres.                                                          |
+--------------+--------------------------------------------------------------------------------------------------------------+
| height       | Stem height, in units of metres.                                                                             |
+--------------+--------------------------------------------------------------------------------------------------------------+
| FRI          | Fire return interval, in years. For example, a plot with an FRI treatment of 3 was burned every three years. |
+--------------+--------------------------------------------------------------------------------------------------------------+

Download the data :download:`here <../../data/marondera.csv>`.

Each plot has an area of is 60 x 36 m, which is 0.216 hectares.

Data preparation
~~~~~~~~~~~~~~~~

First, load in ``marondera.csv`` using the ``read.csv()`` function.

Use the ``head()`` and ``summary()`` functions to get an understanding of the data:

#. How many plots are there?
#. How many experimental fire treatments?
#. What species are present?

This is real data, so it does include errors, which will impact the quality of results if not taken care of. Try to identify if there are any incorrect records:

#. Are all the DBH records realistic?
#. Are the species names all correct and well-ordered? 

.. NOTE:: Hint 1: Any measurement of a tree with a DBH of greater than 100 cm is probably an error. Hint 2: Some of the species names are mispelled, use the ``unique`` function to find out what they are.

What can you do to improve incorrect records?

.. NOTE:: Hint 1: Consider using ``NA`` values. Hint 2: Recall that ``silvR`` can perform corrections to species names.

Stem attributes
~~~~~~~~~~~~~~~

Try and determine the following:

#. How many species are there are there across all plots? 
#. Draw a histogram (in the form ``hist(DBH)``) to show the stem size distribution.
#. Draw a scatter plot to determine if there's a relationship between DBH and stem height.
#. Use silvR to load estimates of wood density for each stem.

Plot attributes
~~~~~~~~~~~~~~~

Determine the structural attributes of each of the plots. Use ``silvR`` to calculate the following parameters for each tree:

#. Stocking density
#. Basal area
#. Aboveground biomass

.. NOTE::
    Remember that these are best presented on a per hectare basis. Recall that each plot at Marondera is 0.216 ha.

Then use the ``aggregate`` function to produce an estimate of each of these parameters at plot scale.

.. NOTE::
    Are all of these plot-scale numbers reasonable? If not, you may need to perform additional data cleaning.

Use silvR to identify the dominant three species, and to calculate biodiverstiy indices for each plot.

The impact of fire return interval on vegetation structure
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Summarise the plot structure parameters you just calculated by fire treatment.

#. How does stocking density, basal area, and biomass vary with FRI?
#. Can a miombo woodland with annual fires support any aboveground biomass?

These data are probably better expressed graphically. Try to build a boxplot of fire return interval vs the structural parameters. The syntax to build a boxplot looks like:

.. code-block:: R
    
	boxplot(biomass~FRI, data=plot_data)


Advanced: The effect of fire on species composition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. How does species composition vary by FRI treatment? (Hint: you can express this as the top-5 species under each treatment)
#. Is there any relationship between biodiversity and FRI?

.. WARNING:: 
    These questions are quite tricky. Consider using the ``table`` function to produce a summary.
	
.. image:: images/fire_kilwa.jpg
   :width: 400px
   :align: center
What next?
==========

Applying functions to your own data
-----------------------------------

Up to this point we've been using synthetic forest plot data. Real life forest plot data will be harder to use: there will be formatting issues, spelling mistakes, and transcription errors to deal with. The key to getting good results using the silvR functions (or any other R library) will be to making sure that your data are in the correct format and have undergone appropriate cleaning.

When you feel ready, arrange your data into a format similar to the sample data in silvR and load it into R (using ``read.csv()``). Try and run through the worked examples, but this time with your own data.

New functionality
-----------------

New functions will be added to silvR over time. If there's something you'd like to see added, please do let us know! Contact sam.bowers@ed.ac.uk.

Other libraries of interest
---------------------------

silvR is just a small part of a large ecosystem of R libraries. If you have a statistical problem, it's very likely that there exists an R library to help with it. Libraries that have been particularly helpful to us inclde:

#. vegan
#. BIOMASS
#. taxize
#. ggplot2

How do I learn more?
--------------------

Learning to program is mostly a matter of practice. There are lots of free resources available online to assist in learning R. Some we can recommend are:

#. https://www.datacamp.com/courses/free-introduction-to-r
#. https://www.youtube.com/playlist?list=PL6gx4Cwl9DGCzVMGCPi1kwvABu7eWv08P
#. https://ourcodingclub.github.io/
#. https://r4ds.had.co.nz/

Happy coding!
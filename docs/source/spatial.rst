Working with spatial data
=========================

Locations of forest inventory plots are usually marked by points measured with a GPS unit. 

There exists a wealth of freely available data from remote sensing and modelling which can be used to better characterise your plots. With silvR and R we can take advantage of this data, and integrate forest inventory measurements with global data products.

Loading sample data
~~~~~~~~~~~~~~~~~~~

For this part of the lirbary, we'll use a dataset that describes the plots and their locations. If not already present, load them into memory using:

.. code-block:: R
    
    > data(mplots)

Managing point data
~~~~~~~~~~~~~~~~~~~

Spatial data can be recorded in a wide range of coordinate reference systems (CRS). You'll probably be familiar with data provided as latitude and longitude (often WGS84) or Universal Transverse Mercator (UTM), (most of Zimbabwe sits in UTM zone 35S).

It can be complex to describe a coordinate reference system in R, but there is a shortcut known as EPSG codes. The most commonly used CRSs have each been given a unique code, which silvR has been programmed to understand. For example, WGS84 has the EPSG code 4326, and UTM35S has the code 32735. You can find out what EPSG code your CRS has from the website http://www.epsg.org/.

silvR has a function to reproject point data to a different CRS given the EPSG codes to convert from and to. For example, to convert the set of points provided in the ``mplots`` dataset from UTM37S to WGS84 (lat/lon), you can run the follows:

.. code-block:: R
    
    > points_rep <- reprojectPoints(mplots$UTM_E, mplots$UTM_N, CRS_from = 32735, CRS_to = 4326)
    
    > head(points_rep)
      x_coords  y_coords
    1 26.96684 -18.54202
    2 26.94692 -18.46403
    3 27.12489 -18.55811
    4 26.88303 -18.41498
    5 26.99591 -18.74058
    6 27.18346 -18.62647

.. NOTE::
    **For advanced users** The ``rgdal`` package has a specialised CRS object that can also be input into ``reprojectPoints``. silvR has a function to load a CRS object from its epsg code. For example: ``CRS_WGS84 <- getCRSFromEPSG(4326)``. You may want to use this function if combining silvR with more advanced spatial processing with ``rgdal``.

Extracting data from Rasters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Raster data consists of an array of cells (a.k.a pixels) arranged into rows and columns. These data can be georeferenced, such that each pixel represents a known location on the Earth. Common examples of raster data are remote sensing images or modelled meteorological data. A commonly used example of raster data is the GeoTiff format. 

Combining forest inventory measurements with raster data opens a lot of opportunities. For example, we might want to determine meteorological parameters for each plot (temperature, precipitation, annual variation), land cover types (grassland, savannah, forest), or generate calibration data for creating our own remote sensing products such as biomass maps.

For the purposes of silvR, we've prepared a test dataset which contains a count of the number of burn scars detected by MODIS in southern Africa over the period 2001 - 2018 at 1 km resolution. Download the dataset :download:`here <../../data/firecount_2001_2018.tif>` (2 MB), and save it somewhere memorable (e.g. Desktop).

.. image:: images/MODIS_fire.PNG
   :width: 400px
   :align: center

To extract pixel values for locations of forest plots, we can use the ``extractFromRaster`` function:

.. code-block:: R
    
    > mplots$fire_count <- extractFromRaster('path/to/firecount_2001_2018.tif', mplots$UTM_E, mplots$UTM_N, EPSG = 32735)
    
    > mplots$fire_count
     [1] 0 0 0 0 0 1 0 1 1 0 2 1 4 0 5 4 1 7 0 0 0 1
    [23] 0 0 1

``extractFromRaster`` will understand a range of geospatial raster data types, including GeoTiffs, HDF files, and VRT files. Feel free to experiment with other data sources.

Extracting data from Vectors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The other commonly used geospatial data type is vector data, which is constructed of points, lines, and polygons. Commonly used vector data formats include shapefiles. Vector datasets can contain, for example, data on locations of protected areas, soil types, and political boundaries such as regions and districts.

.. WARNING:: 
    There is not at present an ``extractFromVector`` function in silvR... but watch this space!
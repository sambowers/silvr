Structural parameters
=====================

Loading silvR and sample data
-----------------------------

First, make sure that you've followed the setup instructions. You'll also need to load in the sample data included in silvR to follow along with these examples, which can be performed with:

.. code-block:: R
    
    > data(mstems) # Stem-scale data
        
    > data(mplots) # Plot-scale data
	
If you've not yet installed silvR, return to the setup instructions before continuing.

.. NOTE::
    All functions and datasets in silvR come with documentation. Where in doubt, get help using the ``?`` character. e.g. ``?calculateBasalArea`` or ``?calculateBiomass``.

Calculating structural parameters
---------------------------------

Structural parameters are calculated at the level of an individual tree (e.g. basal area, biomass), but are usually expressed at the scale of a plot. Here we'll run through the calculation of these parameters a the scale of a stem, then show you how to summarise these at the scale of a plot.

Basal area
----------

Basal area is the area of land that is occupied by the cross-section of a tree. 

.. image:: images/basal_area.png
   :width: 400px
   :align: center

We can calculate basal area from a tree's diameter at breast height (DBH), which is usually measured at 1.3 m above ground level. Assuming a circular tree bole, we can calculate basal area using the equation:

BA = π(DBH/2)^2

To calculate basal area using silvR, we can input a vector of tree DBH measurements into the following function:

.. code-block:: R
    
	> calculateBasalArea(mstems$DBH)
	   [1] 0.030171856 0.633348221 0.012468981 0.089727028 0.030790750 0.296091966 0.014957123 0.026015529
	   [9] 0.014957123 0.133316626 0.258769845 0.658993041 0.010207035 0.051471854 0.029559245 0.134614104
	  [17] 0.024884555 0.057255526 0.009503318 0.009503318 0.012076282 0.033329156 0.244544714 0.011689866
	  [25] 0.423137973 0.115811672 0.009503318 0.022698007 0.009852035 0.468084739 0.092940877 0.094024727
	  [33] 0.026015529 0.185507905 0.048305129 0.053092916 0.079422604 0.084496276 0.021124069 0.012076282
	  [41] 0.015393804 0.309748469 0.011689866 0.541060795 0.020106193 0.246300864 0.012468981 2.301958035
	  [49] 0.021124069 0.899202357 0.446511422 0.030171856 0.087615878 0.055571632 0.031415927 0.013684778
	  ...

You can save the result to the original data.frame by defining a new column as follows:

.. code-block:: R
    
    > mstems$basal_area <- calculateBasalArea(mstems$DBH)

For all of the following examples we'll add the data to the existing ``mstems`` data.frame.

.. NOTE::
    By default, silvR functions such as ``calculateBasalArea`` require DBH to be given in units of ``cm`` and provides outputs in standard units (e.g. ``m^2`` for basal area). Check documentation for other options.

Stem volume
-----------

Stem volume provides an estimate of the ammount of space that the tree bole occupies.

.. image:: images/stem_volume.png
   :width: 400px
   :align: center
   
Stem volume is calculated using the equation of **[SOURCE]**. Calculate it using silvR as follows:

.. code-block:: R
    
    > mstems$volume <- calculateStemVolume(mstems$DBH)

Stocking density
----------------

We can use silvR to calculate stocking density of stems greater than a minimum DBH. At the scale of a stem, this translates to values of 1 (included) and 0 (excluded).

To calculate whether a tree contributes to the stocking count in silvR:

.. code-block:: R
    
    > mstems$stocking <- calculateStocking(mstems$DBH)

To do the same, but with a minimum DBH of 10 cm:

.. code-block:: R
    
    > mstems$stocking_10 <- calculateStocking(mstems$DBH, min_DBH = 10)

Aboveground biomass
-------------------

silvR is pre-programmed with a range of allometric equations that are valid for miombo woodlands. By default, silvR will use the ``Ryan11`` model described in this paper:

*Ryan, Casey M., Mathew Williams, and John Grace. "Above‐and belowground carbon stocks in a miombo woodland landscape of Mozambique." Biotropica 43.4 (2011): 423-432.*

Run this in silvR using:

.. code-block:: R
    
    > mstems$AGB_Ryan11 <- calculateBiomass(mstems$DBH)

Recall that we can view summary statistics using the R ``summary`` function:
    
.. code-block:: R
    
    > summary(mstems$AGB_Ryan11)
    Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
    0.003714 0.014319 0.050140 0.218266 0.183587 7.424314

We can also use other allometric models. For example:

.. code-block:: R
    
    > mstems$AGB_Mugasha14 <- calculateBiomass(mstems$DBH, model = 'Mugasha14')
    > mstems$AGB_Mutakele09<- calculateBiomass(mstems$DBH, model = 'Mutakele09')

See documentation (``?calculateBiomass``) for all allometric models.

.. NOTE::
    Most allometric models only require ``DBH`` as input to calculate DBH, but be aware that some may require height or even species information. Some of these more advanced models for estimating aboveground biomass are used in silvR (``Chave05`` and ``Chave14``), but we'll return to these in the next section.
	
Aggregating structural parameters to plot scale
-----------------------------------------------

Most commonly we will want to express these structural parameters at plot scale. For example, basal area is usally expressed as the sum total of basal area of all trees, in units of m^2/ha.

We can use functions from base R to aggregate by plot:

.. code-block:: R
    
    > ba_plot <- aggregate(basal_area ~ plot_code, mstems, sum)
    
    > ba_plot$basal_area <- ba_plot$basal_area / 0.25 # Because each plot is only 0.25 ha in size, and we want units of m^2/ha
    
    > head(ba_plot)
      plot_code basal_area
    1   plot_01   25.08409
    2   plot_02   19.17778
    3   plot_03   30.12648
    4   plot_04   43.59910
    5   plot_05   27.74461
    6   plot_06   21.80926

Try to repeat this process to calculate stocking density and aboveground biomass at each plot.
Species functions
=================

Formatting species data
-----------------------

Species data tend to come in the format ``Genus species``, but most functions in silvR require separate vectors for ``Genus`` and ``species``. 

silvR has a function to split character strings in the format ``Genus species`` or ``Genus.species`` into two vectors, and enforces correct capitalisation:

.. code-block:: R
    
    > splitGenusSpecies(mstems$species_name)
              genus        species
    1      Parinari curatellifolia
    2    Terminalia        sericea
    3       Afzelia     quanzensis
    4 Erythrophleum      africanum
    5    Terminalia        sericea
    6  Brachystegia    spiciformis
    ...

These can be attached to the original data.frame using the ``cbind`` function from base R to be used as inputs to other functions:

.. code-block:: R
    
    > mstems <- cbind(mstems, splitGenusSpecies(mstems$species_name))
    
    > head(mstems$genus) # Show the first few lines of the new field
	[1] "Parinari"      "Terminalia"   
    [3] "Afzelia"       "Erythrophleum"
    [5] "Terminalia"    "Brachystegia" 

Correcting species data
-----------------------

On occasion we'll want to group species by order or family, requiring time consuming work. Getting consistent spellings in forest inventory data is a further challenge, yet necessary in order to produce accurate assessments of species distributions and biodiversity. There already exist R packages that are capable of automating such spelling corrections (e.g. ``BIOMASS`` and ``taxize``), in silvR we have aimed to simplify some of this processing based on functions from the ``BIOMASS`` package.

The function ``getTaxonomicNames`` has multiple purposes. First, it can identify order and family given a genus:

.. code-block:: R
    
    > taxonomy <- getTaxonomicNames(mstems$genus)
    
    > head(taxonomy)
         order           family         genus species
    1 Malpighiales Chrysobalanaceae      Parinari    <NA>
    2     Myrtales     Combretaceae    Terminalia    <NA>
    3      Fabales         Fabaceae       Afzelia    <NA>
    4      Fabales         Fabaceae Erythrophleum    <NA>
    5     Myrtales     Combretaceae    Terminalia    <NA>
    6      Fabales         Fabaceae  Brachystegia    <NA>

If we're only interested in one taxonomic rank, we can specify it using the ``ranks`` input:

.. code-block:: R
    
    > mstems$family <- getTaxonomicNames(mstems$genus, ranks = c('family')) # Add to original data.frame
    
    > head(mstems$family)
    [1] "Chrysobalanaceae" "Combretaceae"     "Fabaceae"         "Fabaceae"         "Combretaceae"    
    [6] "Fabaceae"

``getTaxonomicNames`` can also search through species databases and perform automated correcion of species names using the ``clean_input`` option:

.. code-block:: R
    
    > species_corrected <- getTaxonomicNames(mstems$genus, species = mstems$species, clean_input = TRUE)
    
    > head(species_corrected)
             order           family         genus        species
    1 Malpighiales Chrysobalanaceae      Parinari curatellifolia
    2     Myrtales     Combretaceae    Terminalia        sericea
    3      Fabales         Fabaceae       Afzelia     quanzensis
    4      Fabales         Fabaceae Erythrophleum      africanum
    5     Myrtales     Combretaceae    Terminalia        sericea
    6      Fabales         Fabaceae  Brachystegia    spiciformis

To also show which species names have been changed, use the ``return_changes`` option which will return a ``nameModified`` field:

.. code-block:: R
    
    > species_corrected <- getTaxonomicNames(mstems$genus, species = mstems$species, clean_input = TRUE, return_changes = TRUE)
    
    > head(species_corrected)
             order           family         genus        species nameModified
    1 Malpighiales Chrysobalanaceae      Parinari curatellifolia        FALSE
    2     Myrtales     Combretaceae    Terminalia        sericea        FALSE
    3      Fabales         Fabaceae       Afzelia     quanzensis        FALSE
    4      Fabales         Fabaceae Erythrophleum      africanum        FALSE
    5     Myrtales     Combretaceae    Terminalia        sericea        FALSE
    6      Fabales         Fabaceae  Brachystegia    spiciformis        FALSE


.. NOTE::
    We advise that you use these functions with caution and don't rely on their outputs 100%; they can occasionally make mistakes leaving you with records of species that don't actually exist in your inventory plots.

Loading wood density
--------------------

Wood density is a useful plant trait, and is required by some allometric equations to predict tree biomass. Wood density is species specific, and can be loaded from existing databases. silvR uses the ``BIOMASS`` library to produce estimates of wood density.

At it's simplest, this function requires a genus as input:

.. code-block:: R
    
    > mstems$wood_density <- loadWoodDensity(mstems$genus)

We can get better estimates with both genus and species:

.. code-block:: R
    
    > mstems$wood_density <- loadWoodDensity(mstems$genus, species = mstems$genus)

In some cases a species will not be present in the wood density database. For these cases, we can also input family:

.. code-block:: R
    
    > mstems$wood_density <- loadWoodDensity(mstems$genus, species = mstems$genus, family = mstems$family)

For cases where family is also not enough to retrieve a wood density, we can input the plot code for each stem which will apply the average wood density for each plot to unknown species:

.. code-block:: R
    
    > mstems$wood_density <- loadWoodDensity(mstems$genus, species = mstems$genus, family = mstems$family, plot = mstems$plotcode)

.. NOTE::
    By default silvR assumes a location of tropical Africa. If you're working on data from outside Africa, take a look at at the documentation using ``?loadWoodDensity`` and the ``region`` option.    

Esimating aboveground biomass
-----------------------------

For some more complex allometric equations, species names and tree height are used to extract wood density estimates. An example of these are the 'Chave' allometric equations applicable to tropical trees, described in:

#. Chave, Jérôme, et al. "Tree allometry and improved estimation of carbon stocks and balance in tropical forests." Oecologia 145.1 (2005): 87-99.
#. Chave, Jérôme, et al. "Improved allometric models to estimate the aboveground biomass of tropical trees." Global change biology 20.10 (2014): 3177-3190.

To use these equations in silvR, we'll return to the ``calculateBiomass`` function, using additional inputs describing species:

.. code-block:: R
    
    > mstems$AGB_Chave05 <- calculateBiomass(mstems$DBH, height = mstems$height, model = 'Chave05', family = mstems$family, genus = mstems$genus, species = mstems$species)
    
    > mstems$AGB_Chave14 <- calculateBiomass(mstems$DBH, height = mstems$height, model = 'Chave14', family = mstems$family, genus = mstems$genus, species = mstems$species)

See ``?calculateBiomass`` for more information.

Identifying dominant species
----------------------------

A simple way of summarising species data is to identify the dominant species at each plot.

By default, the function identifies 5 species by stocking density at each plot:

.. code-block:: R
    
    > dominant_species <- getDominantSpecies(mstems$species_name, mstems$plot_code)
	
	> head(dominant_species)
		  plot_code            dominant_sp_1            dominant_sp_2            dominant_sp_3
	1   plot_01   Isoberlinia angolensis Brachystegia spiciformis  Julbernardia globiflora
	2   plot_02 Brachystegia spiciformis   Isoberlinia angolensis  Erythrophleum africanum
	3   plot_03           Faurea saligna   Isoberlinia angolensis Brachystegia spiciformis
	4   plot_04   Isoberlinia angolensis Brachystegia spiciformis  Julbernardia globiflora
	5   plot_05   Isoberlinia angolensis Brachystegia spiciformis          Burkea africana
	6   plot_06  Julbernardia globiflora Brachystegia spiciformis  Erythrophleum africanum
				dominant_sp_4               dominant_sp_5 dominant_sp_mt_1 dominant_sp_mt_2 dominant_sp_mt_3
	1 Parinari curatellifolia Diplorhynchus condylocarpon               21               13               13
	2          Faurea saligna        Anisophyllea boehmii               13                9                8
	3 Julbernardia globiflora Diplorhynchus condylocarpon               18               18               17
	4    Anisophyllea boehmii             Burkea africana               24               22               20
	5      Marquesia macroura        Brachystegia boehmii               16               15               11
	6         Burkea africana        Dombeya rotundifolia               14               10               10
	  dominant_sp_mt_4 dominant_sp_mt_5 dominant_sp_pc_1 dominant_sp_pc_2 dominant_sp_pc_3 dominant_sp_pc_4
	1               13               11         13.20755         8.176101         8.176101         8.176101
	2                8                7         10.83333              7.5         6.666667         6.666667
	3               17               13         7.725322         7.725322         7.296137         7.296137
	4               17               15         9.125475         8.365019         7.604563         6.463878
	5               11                9          9.69697         9.090909         6.666667         6.666667
	6                9                9         9.090909         6.493506         6.493506         5.844156
	  dominant_sp_pc_5
	1         6.918239
	2         5.833333
	3         5.579399
	4         5.703422
	5         5.454545
	6         5.844156

.. NOTE::
    This function uses complete species names (i.e. *Genus species*) in place of individual genus and species vectors.
	
The data.frame output by this function is complex. Columns ``dominant_sp_#`` show the dominant species for each plot (from 1 to n), ``dominant_sp_mt_#`` show the value of the comparison metric (in this case stocking density), and ``dominant_sp_pc_#`` the percentage of the comparison metric by each species.

We can combine this function with structural information, such as basal area or biomass. For example:

.. code-block:: R

    > dominant_species <- getDominantSpecies(mstems$species_name, mstems$plot_code, metric = calculateBasalArea(mstems$DBH))

or aboveground biomass

.. code-block:: R

    > dominant_species <- getDominantSpecies(mstems$species_name, mstems$plot_code, metric = calculateBiomass(mstems$DBH, model = 'Ryan11'))

Calculating biodiversity
------------------------

Biodiversity describes the varaibility of species in a forest inventory plot. Measures of biodiversity vary from very simple (species richness) to more complex measures based on the distribution of individuals amongst species.. These measures of diveristy are collectively called 'diversity indices'.

silvR is pre-programmed with a range of diversity indices (see documentation at ``?calculateBiodiversity``). 

To calculate species richness:

.. code-block:: R
    
    > calculateBiodiversity(mstems$species_name, mstems$plot_code, index = 'richness')
    plot_01 plot_02 plot_03 plot_04 plot_05 plot_06 plot_07 plot_08 plot_09 plot_10 plot_11 plot_12 plot_13 
        25      26      27      27      25      25      25      25      22      20      25      26      15 
    plot_14 plot_15 plot_16 plot_17 plot_18 plot_19 plot_20 plot_21 plot_22 plot_23 plot_24 plot_25 
        22      26      25      25      25      26      26      23      24      20      23      24 

For a more complex measure of diveristy, use the 'simpson' or 'shannon' indices:

.. code-block:: R
    
    > calculateBiodiversity(mstems$species_name, mstems$plot_code, index = 'shannon')
     plot_01  plot_02  plot_03  plot_04  plot_05  plot_06  plot_07  plot_08  plot_09  plot_10  plot_11 
    2.930107 3.025561 3.117128 3.089926 3.009947 3.088370 3.060124 2.978967 2.887835 2.767140 3.012447 
    plot_12  plot_13  plot_14  plot_15  plot_16  plot_17  plot_18  plot_19  plot_20 plot_21  plot_22 
    2.984938 2.440725 2.917779 2.954540 3.023698 3.015096 2.959135 3.062585 3.005983 2.944163 2.998152 
    plot_23  plot_24  plot_25 
    2.815136 2.944381 2.940585 

If you want to calculate diodiversity based on another metric, such as basal area, use the ``abundance`` option:

.. code-block:: R
    
    calculateBiodiversity(mstems$species_name, mstems$plot_code, index = 'shannon', abundance = calculateBasalArea(mstems$DBH))
     plot_01  plot_02  plot_03  plot_04  plot_05  plot_06  plot_07  plot_08  plot_09  plot_10  plot_11 
    2.645848 2.846774 3.063116 2.854390 2.710108 2.982382 2.983830 2.747758 2.654970 2.366331 2.973090 
    plot_12  plot_13  plot_14  plot_15  plot_16  plot_17  plot_18  plot_19  plot_20  plot_21  plot_22 
    2.865765 1.988800 2.796020 2.862064 2.613919 2.917068 2.728365 2.991442 2.868902 2.707835 2.750818 
    plot_23  plot_24  plot_25 
    2.435320 2.654030 2.455297

Preparing data for vegan (advanced)
-----------------------------------

If you're interested in assessment of biodiversity and species distributions, we can recommend looking at the ``vegan`` package in R, which provides a wide variety of very useful functions. Many functions in ``vegan`` require data to be arranged in a different format to silvR.

To convert to a vegan applicable format, run:

.. code-block:: R
   
   mstems_vegan <- prepareForVegan(mstems$species_name, mstems$plot_code)

See ``?prepareForVegan`` for further options.
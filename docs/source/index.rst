.. silvR documentation master file, created by
   sphinx-quickstart on Tue Feb 12 14:24:52 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

silvR: An R library for forest inventory data
=============================================

.. image:: images/silvR_logo.png
   :width: 500px
   :align: center

silvR is a set of R functions for analysing forest inventory data.

In this document we introduce the silvR functions, and run through a set of worked examples with synthetic tree inventory data. We recommend you learn how the silvR functions work, and then try to input your own data.

Who do I talk to?
-----------------

Repo maintained by Samuel Bowers (`sam.bowers@ed.ac.uk <mailto:sam.bowers@ed.ac.uk>`_).

Contents
--------

.. toctree::
   :maxdepth: 1
   
   setup.rst
   intro.rst
   structure.rst
   species.rst
   spatial.rst
   what_next.rst
   marondera.rst
   

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

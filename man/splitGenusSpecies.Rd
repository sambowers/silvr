% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/species.R
\name{splitGenusSpecies}
\alias{splitGenusSpecies}
\title{Separate spcecies names (Genus, species) into two vectors}
\usage{
splitGenusSpecies(species_name)
}
\arguments{
\item{species_name}{Vector of species names in format 'Genus species'}
}
\value{
A data frame containing genus and species columns
}
\description{
Names separated by ' ' or '.' are split, and appropriate capitalisation applied.
}
\examples{
splitGenusSpecies(c('Brachystegia spiciformis', 'Julbernardia globiflora'))

}
\author{
Samuel Bowers
}

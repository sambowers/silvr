Introduction
============

What is silvR?
--------------

silvR is an R package. Packages are sets of functions, data and documentation that is easy to share with others. R has many thousands of packages that are publically and freely available. If you have a statistical problem, chances are someone has already solved it and published a package that you can use.

silvR has been developed to assist in the analysis of forest inventory plot data, with a focus on problems that are specific to southern African woodlands. silvR has been developed as part of the SEOSAW project (https://seosaw.github.io/), which has brought together plot inventory data from across southern Africa.

The package is still under development, so if you have any suggestions for changes or for new features, do let us know!

Required data structure
-----------------------

We strongly advise that you organise your data in the following standardised way. By organising forest inventory data as we describe here, you'll be able to make best use of the functions of silvR, but also find that your data will be easier to apply other common R functions.

Functions in silvR work on two scales: individuals and plots. Example functionality that is applicable to the individual scale is:

* Calculating stem biomass
* Correction of species names

...and functionality that is applicable to the plot scale:

* Calculating plot biomass
* Characterisation of dominant species
* Extracting data from remote sensing products

We recommend you maintain two R ``data.frames``: one containing individual stem data, and one containing plot data. There should be a ``plotcode`` field in both data.frames, so that individual stem data can be linked to properties of the plot it was measured at.

Your ``individual stem`` data.frame should be similar to:

+----------+-----------------------------+------+--------+
| plotcode | species_name                | DBH  | height |
+----------+-----------------------------+------+--------+
| P01      | Brachystegia spiciformis    | 10.5 | 7.5    |
+----------+-----------------------------+------+--------+
| P01      | Julbernardia globiflora     | 22.1 | 10.0   |
+----------+-----------------------------+------+--------+
| P02      | Diplorhynchus condylocarpon | 15.4 | 8.0    |
+----------+-----------------------------+------+--------+
| P02      | Brachystegia spiciformis    | 31.9 | 12.1   |
+----------+-----------------------------+------+--------+
| P03      | Afzelia quanzensis          | 48.8 | 20.2   |
+----------+-----------------------------+------+--------+

Your ``plot`` data.frame should be similar to:

+----------+----------+-----------+---------+
| plotcode | latitude | longitude | area_ha |
+----------+----------+-----------+---------+
| P01      | -18.967  | 26.332    | 0.25    |
+----------+----------+-----------+---------+
| P02      | -19.498  | 26.635    | 0.25    |
+----------+----------+-----------+---------+
| P03      | -19.011  | 27.090    | 0.25    |
+----------+----------+-----------+---------+

.. note:: At minimum your ``stem`` data.frame should contain a plotcode, species_name and DBH, and your ``plot`` data.frame should contain a plotcode, latitude, and longitude. Other fields describing individuals (e.g. height, stem location) or plots (e.g. vegetation type, meteorological data) should also be included where available.

Using bundled data
------------------

silvR comes bundled with two datasets that can be used to test out its functions. These data are for synthetic (*i.e. totally fictional!*) forest plot data in miombo woodlands. These are loaded with:

.. code-block:: R
    
    > data(mstems) # Stem-scale data
        
    > data(mplots) # Plot-scale data

Cleaning your own data
----------------------

At present, there is only limited functionality in silvR to check and clean input data, meaning that you will have to make sure that input data are sensible. Where there are transciprion errors in your data, silvR will not necessarily be able to identify them. Checks we can recommend are:

* Are all DBH measurements realistic, and in the same units (cm or m)?
* Are there an equal number of unique plot codes in your ``stem`` and ``plot`` data.frames?
* Are plot locations all given in the same coordinate reference system (e.g. WGS 84)?
* Are all species names sensible, and without spelling mistakes? N.B. There is a function in silvR that performs basic species name corrections.


